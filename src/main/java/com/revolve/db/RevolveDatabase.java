package com.revolve.db;

import com.revolve.objects.SystemMessages;

import javax.jdo.JDOHelper;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

@SuppressWarnings("JpaQlInspection")
public class RevolveDatabase implements Serializable {

    // Lets use singleton pattern, for performance reasons
    private static RevolveDatabase instance;

    public static RevolveDatabase getInstance() {
    //    if (instance == null) {
    //        instance = new RevolveDatabase();
    //    }
        return instance;
    }
    
    public static RevolveDatabase getInitialInstance() {
        instance = new RevolveDatabase();
        return instance;
    }

    private EntityManagerFactory emf;
    private EntityManager em;
    private int PORT = 4504;


    private RevolveDatabase() { // Please use RevolveDatabase.getInstance() to obtain instance
        /**
         * Get the client properties and get the port for the database
         */

        // This should be initialized only once, not on every call, so lets move it here instead in openDB
        Map<String, String> properties = new HashMap<String, String>();
        properties.put("javax.persistence.jdbc.user", "admin");
        properties.put("javax.persistence.jdbc.password", "admin");
        emf = Persistence.createEntityManagerFactory("objectdb://localhost:" + PORT + "/821.odb", properties);
    }

    /**
     * Open the Entity Manager for Persistence ObjectDB is database
     */
    private void openDB(int s) {
        try {
            em = emf.createEntityManager();
        } catch (Exception e) {
            closeDB();
            e.printStackTrace();
        }
    }
    /**
     * Close the database
     */
    public void closeDB() {
        try {
            if (em.isOpen()) {
                em.close();
            }
//            if(emf.isOpen()) emf.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void addSystemMessages(SystemMessages systemMessages) {
        openDB(18);
        boolean foundSw = false;
        List<SystemMessages> lists = null;
        try {
            try {
                lists = em.createQuery("SELECT c FROM SystemMessages c where c.messageId='" + systemMessages.getMessageId() + "'", SystemMessages.class).getResultList();
                /**
                 * If message is already recorded then end
                 */
                if (lists.size() > 0) {
                    return;
                    //em.getTransaction().begin();
                    //em.createQuery("DELETE FROM SystemMessages c where c.messageId='" + systemMessages.getMessageId() + "'").executeUpdate();
                    //em.getTransaction().commit();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            em.getTransaction().begin();
            em.persist(systemMessages);
            em.getTransaction().commit();
        } catch (Exception e) {
            closeDB();
        }
        closeDB();
    }

    public List getSystemMessages() {
        openDB(19);
        List<SystemMessages> messages = null;
        TypedQuery<SystemMessages> query = em.createQuery("SELECT c FROM SystemMessages c", SystemMessages.class);
        try {
            messages = query.getResultList();
        } catch (Exception e) {
            closeDB();
            return messages;
        }
        closeDB();
        return messages;
    }

    public void deleteSystemMessages(String mess) {
        openDB(20);
        try {
            em.getTransaction().begin();
            em.createQuery("DELETE FROM SystemMessages c where c.messageNo='" + mess + "'").executeUpdate();
            em.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            closeDB();
        }
        closeDB();
    }

}
