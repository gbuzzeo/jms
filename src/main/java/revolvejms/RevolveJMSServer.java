package revolvejms;

public class RevolveJMSServer {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            RevolveJms revolveJms = new RevolveJms(args[0],args[1]);
            revolveJms.run();
        }
        catch(Exception r) {
        }
    }
    
}
