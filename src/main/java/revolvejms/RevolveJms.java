package revolvejms;

import java.io.*;
import java.net.*;
import org.apache.activemq.ActiveMQConnectionFactory;
import javax.servlet.http.*;
import javax.jms.*;
import com.revolve.db.RevolveDatabase;
import com.revolve.objects.SystemMessages;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import org.json.JSONObject;
import java.security.Security.*;
import com.sun.net.ssl.*;

public class RevolveJms implements MessageListener, Runnable {

    private RevolveDatabase db;
    Connection connection;
    ActiveMQConnectionFactory connectionFactory;
    String hostName = "";
    String tcpName = "";

    public RevolveJms(String host, String tcp) {
        db = RevolveDatabase.getInitialInstance();
        hostName = host;
        tcpName = tcp;
        //https://devpresentation.revolveplatform.com/RevolveNoMenu/broadcast/?client=bfapp&message=%22test%22
        System.out.println("Broadcaster located at " + "http://" + hostName + "/broadcast");
        System.out.println("tcp located at " + tcpName);
        connectionFactory = new ActiveMQConnectionFactory("tcp://" + tcpName + ":61616");
        //test(host,"Gene");
    }

    public void run() {
        try {
            connection = connectionFactory.createConnection();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Destination destination = session.createQueue("system.messages");
            MessageConsumer responseConsumer = session.createConsumer(destination);
            responseConsumer.setMessageListener(this);
            connection.start();
            System.out.println(">>>Connection: " + connection.getClientID());
        } catch (Exception e) {
            System.out.println("Server aborted..." + e.getMessage());
            run();
        }
    }

    public void onMessage(Message message) {
        String messageText = null;
        String res = "Unknown";
        try {
            if (message instanceof TextMessage) {
                TextMessage textMessage = (TextMessage) message;
                messageText = textMessage.getText();
            } else if (message instanceof BytesMessage) {
                BytesMessage byteMessage = (BytesMessage) message;
                byte[] buffer = new byte[(int) byteMessage.getBodyLength()];
                byteMessage.readBytes(buffer);
                messageText = new String(buffer);
            }
            System.out.println(messageText);
            /**
             * Write the message to the database
             */
        } catch (Exception e) {
        }
        JSONObject obj = new JSONObject();
        try {
            obj = new JSONObject(messageText);
            if (obj.getString("results").equals("0")) {
                res = "Rejected";
            }
            if (obj.getString("results").equals("1")) {
                res = "Approved";
            }
        } catch (Exception e) {
        }
        try {
            /*
{
      "body": "Application:143221802550201710 For Customer Email: gene1095@buzz.com Was Denied.",
      "user_id": "141455290946037210",
      "message_id": "143223379696609716",
    "clinic_id": "141701178931326616",
      "client_id": "141455290524914916",
      "level": "clinic",
      "priority": "high",
        "type": "trigger",
      "application_id": "143221802550201710",
      "results": "0"
}
            
              "body": "https://avant-us-pos-web.herokuapp.com/apply/leads?auth_token=MqHFt6MQ4CBxDQckGc1z|customer_application_id=4268|lead_id=2297|lead_provider=bestfriendscredit|welcome_id=208",
    "user_id": "143126993701201512",
    "client_id": "141455290524914916",
    "level": "tablet",
    "priority": "high",
    "type": "trigger",
    "application_id": "143264291995405713",
    "message_id": "143264330577410417",
    "results": "1"
            */
            db = RevolveDatabase.getInstance();
            Date date = Calendar.getInstance().getTime();
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            SystemMessages sm = new SystemMessages();
            sm.setApplicationId(obj.getString("application_id"));
            sm.setBody(obj.getString("body"));
            sm.setClientId(obj.getString("client_id"));
            try {
                sm.setClinicId(obj.getString("clinic_id"));
            }
            catch(Exception e) {
            }
            sm.setLevel(obj.getString("level"));
            sm.setMessageDate(sdf.format(date));
            sm.setMessageId(obj.getString("message_id"));
            sm.setMessageRead(false);
            sm.setPriority(obj.getString("priority"));
            sm.setResults(res);
            sm.setType(obj.getString("type"));
            sm.setUserId(obj.getString("user_id"));
            if(sm.getLevel().equals("clinic")) {
                db.addSystemMessages(sm);
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error in JSON " + messageText);
        }
        
        try {
            System.out.println("Message=" + messageText);
            URL url = new URL("http://" + hostName + "/broadcast");
            System.out.println("Sending URL - " + url.toString());
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            
//HttpsURLConnection con = (HttpsURLConnection)myurl.openConnection();
//con.setRequestMethod("POST");

           // conn.setRequestProperty("Content-length", String.valueOf(query.length())); 
          //  conn.setRequestProperty("Content-Type","application/x-www- form-urlencoded"); 
         //   conn.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0;Windows98;DigExt)"); 
//con.setDoOutput(true); 
//con.setDoInput(true); 

//DataOutputStream output = new DataOutputStream(con.getOutputStream());  


            OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());

            writer.write("message=" + messageText);
            writer.flush();
            String line;
            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }
            writer.close();
            reader.close();
        } catch (Exception e) {
        }
                
    }

    void test(String hostName, String messageText) {
        try {
            System.out.println("Message=" + messageText);
            URL url = new URL("https://" + hostName + "/broadcast");
            System.out.println("Sending URL - " + url.toString());
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());

            writer.write("message=" + messageText);
            writer.flush();
            String line;
            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }
            writer.close();
            reader.close();
        } catch (Exception e) {
        }

    }
}
